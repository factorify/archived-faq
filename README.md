# Knowledge base #

## Frontend ##

* https://angular.io/tutorial

## Backend ##

* https://spring.io/guides/tutorials/bookmarks/
* https://google.github.io/styleguide/javaguide.html
* http://postgresguide.com/

### Java ###
* pro úplné základy doporučuji aktuální kurz na Coursera nebo Udemy
* Effective Java
* Java Concurrency in Practice

## Planning & Scheduling ##

* https://developers.google.com/optimization/scheduling/job_shop
* https://www.dropbox.com/s/61utua2zhso9sgt/reinforc-scheduling-long.pdf?dl=0

## Production domain ##

* https://en.wikipedia.org/wiki/Theory_of_constraints
* https://en.wikipedia.org/wiki/Manufacturing_resource_planning

## Refcards ##

* https://dzone.com/storage/assets/4595-rc008-designpatterns_online.pdf
* https://dzone.com/storage/assets/487673-rc024-corejava.pdf
* https://dzone.com/storage/assets/4082-rc026-springannot_online.pdf
* https://dzone.com/asset/download/206

## SQL ##
* https://www.udemy.com/postgresql-from-zero-to-hero/#instructor-1

